class IssuesController < ApplicationController
  before_action :set_issue, only: [:show]

  def show
    render json: @issue
  end

  def index
    render json: Issue.all
  end

  private
    def set_issue
      @issue = Issue.find(params[:id])
    end
end
