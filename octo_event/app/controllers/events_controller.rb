class EventsController < ApplicationController

  def create
    event = Events::Create.new(params)
    event.call
    if event.success?
      render json: event.result, status: :created
    else
      render json: {errors: event.errors}, status: :unprocessable_entity
    end
  end

  def index
    params[:issue_id] ?  @events = Issue.find_by(number: params[:issue_id]).events : @events = Event.all
    render json: @events
  end
end
