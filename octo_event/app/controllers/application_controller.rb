class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :data_not_found

  private

  def data_not_found
    render json: {errors: "Not found any data!"}, status: :not_found
  end

end
