Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'home/index'
  root to: "home#index"
  resources :events, only: [:create]
  resources :issues, only: [:index, :show] do
    resources :events, only: [:index]
  end
  
end
