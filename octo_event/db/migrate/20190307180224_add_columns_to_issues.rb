class AddColumnsToIssues < ActiveRecord::Migration[5.2]
  def change
    add_column :issues, :number, :integer
    add_column :issues, :github_id, :string
  end
end
