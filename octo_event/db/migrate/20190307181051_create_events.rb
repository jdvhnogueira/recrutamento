class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.timestamps
      t.string :action
      t.belongs_to :issue, foreign_key: true
    end
  end
end
